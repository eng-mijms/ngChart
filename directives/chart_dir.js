define(['../controllers/chart_ctrl'],function () {
    /**
    * ngChartDir Module
    *
    * Description
    */
    angular.module('ngChartDir', ['ngChartCtrl','ngChartServ']).directive('d3Test', ['chartServ', function (chartServ) {
        return {
            restrict: 'A',
            scope:{
                width: '@',
                height: '@',
            },
            link: function (scope, ele, iAttrs) {
                console.log('test');
                var testData = [10,20,30,40,50,60,70,80,90,100,110,120,130,140];


                var width=scope.width || 250;
                var height=scope.height||250;
                console.log(d3.max(testData)*testData.length);
                var maxxScale = d3.max(testData)*testData.length;
                var xScale = chartServ.scaleFunction([0, maxxScale], 0, width)
                var rScale = chartServ.scaleFunction(testData, 0, width)
                var yscale = chartServ.scaleFunction(testData, height, 0)

                var svg = d3.select(ele[0])
                .append('svg')
                .attr("width", width)
                .attr("height", height);

                svg.selectAll('circle')
                .data(testData)
                .enter()
                .append('circle')
                .attr('r', function (d, i) {
                    return xScale(d);
                })
                .attr('cx', function (d, i) {
                    // return xScale(d) + 'px';
                    console.log((i+1) * d);
                    console.log(xScale((i+1) * d));
                    return xScale((i+1) * d) + 'px';
                })
                .attr('cy', function (d, i) {
                    return width/2+'px';
                });


            }
        };
    }])


})
