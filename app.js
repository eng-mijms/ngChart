define(['./controllers/chart_ctrl','./services/chart_serv', './directives/chart_dir'],function () {
    /**
    * ngChart Module
    *
    * Description
    */
    angular.module('ngChart', ['ngChartCtrl','ngChartServ','ngChartDir']);
})
