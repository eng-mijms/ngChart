define([], function () {
    /**
    * ngChartServ Module
    *
    * Description
    */
    angular.module('ngChartServ', [])
    .factory('chartServ', [function () {
        var out = {};
        out.scaleFunction = function (data, domainStrart, domainEnd, rangeFromMin, scaleType) {
            // body...
            var types = ['linear', 'pow', 'log']
            var minPoint = rangeFromMin ? d3.min(data):0;
            var maxPoint = d3.max(data);
            var type = scaleType || 'linear';
            if(types.indexOf(type)>-1)
            {
                // debugger
                return d3.scale[type]()
                .domain([minPoint,maxPoint])
                .range([domainStrart , domainEnd]);
            }
            else{
                throw "not supported type , please....check your type in "+types
            }
        }

        return out;
    }])
})
